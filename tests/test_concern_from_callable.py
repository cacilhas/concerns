from unittest import TestCase
from collections.abc import MutableMapping
from datetime import date
from typing import NamedTuple
from concerns import concern


class TestConcernFromCallable(TestCase):

    def test_contract(self):
        person = Person('Dean', 'Winchester')
        today = date.today()
        self.assertFalse(person.contracted)
        person.contract(today)
        self.assertTrue(person.contracted)
        self.assertEqual(person._employee_since, today)


#-------------------------------------------------------------------------------

def make_employee(context: MutableMapping) -> None:
    context.update(
        _employee_since=None,
        contract=lambda self, d: setattr(self, '_employee_since', d),
        contracted=property(lambda self: self._employee_since is not None),
    )


class PersonBase(NamedTuple):
    name: str
    surname: str


class Person(PersonBase):
    concern(make_employee)
