from unittest import TestCase
from typing import NamedTuple
from concerns import concern

class TestConcernFromDict(TestCase):

    def test_fullname(self):
        person = Person('John', 'Doe')
        self.assertEqual(person.fullname, 'John Doe')


#-------------------------------------------------------------------------------

class PersonBase(NamedTuple):
    name: str
    surname: str


class Person(PersonBase):
    concern({
        'fullname': property(
            lambda self: '{0.name} {0.surname}'.format(self).strip()
        ),
    })
