from unittest import TestCase
from abc import ABC, abstractmethod
from collections.abc import Mapping
import json
from typing import NamedTuple, Type
from concerns import concern


class TestConcernFromClass(TestCase):

    def test_serialize(self):
        person = Person(name='John', surname='Doe')
        self.assertEqual(
            person.serialize(),
            '{"name": "John", "surname": "Doe"}',
        )

    def test_deserialize(self):
        person = Person.deserialize('{"name": "J", "surname": "Quest"}')
        self.assertIsInstance(person, Person)
        self.assertEqual(person.name, "J")
        self.assertEqual(person.surname, "Quest")


#-------------------------------------------------------------------------------
class T(ABC):

    @abstractmethod
    def _asdict(self) -> Mapping:
        return NotImplemented

    @classmethod
    def __subclasshook__(cls, C) -> bool:
        return any(hasattr(B, '_asdict') for B in C.__mro__)

T.register(NamedTuple)


class SerialMixin:

    def serialize(self: T) -> str:
        return json.dumps(self._asdict(), sort_keys=True)

    @classmethod
    def deserialize(cls: Type[T], data: str) -> T:
        return cls(**json.loads(data))


class PersonBase(NamedTuple):
    name: str
    surname: str


class Person(PersonBase):
    concern(SerialMixin)
