from collections.abc import Mapping, MutableMapping
from typing import Callable, Union
import inspect

__all__ = ['concern']


# Type aliases
FunctionConcern = Callable[[MutableMapping], None]
Acceptable = Union[FunctionConcern, Mapping, type]


def concern(aspect: Acceptable) -> None:
    """Change the classe behavior based on a mix-in parameter

    If the aspect is a class, the aspect context will be merged to the target
    class context.
    Note: It has been designed to work with user-defined classes.

    If the aspect is a mapping, it itself will be merged to the target class
    context.

    If the aspect is a callable, the target class context will be passed as
    parameter to the aspect for processing.

    Args:
        aspect: A class, mapping or callable used as updating parameters to the
                class context.

    Returns:
        None

    Raises:
        TypeError: if the aspect type is not acceptable (from dict.update())
        Exception: can raise any exception from callable aspect
    """
    context: MutableMapping = inspect.currentframe().f_back.f_locals

    # FunctionConcern
    if is_function_concern(aspect):
        aspect(context)
        return  # nothing else to do here

    # type (a user-defined class)
    if not isinstance(aspect, Mapping):
        aspect = aspect.__dict__  # now it is a dict (Mapping)

    # Mapping
    context.update(aspect)


def is_function_concern(aspect) -> bool:
    if callable(aspect):
        spec = inspect.getfullargspec(aspect)
        return len(spec.args) == 1 \
           and issubclass(spec.annotations[spec.args[0]], MutableMapping)
    return False
