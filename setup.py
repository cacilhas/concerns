#!/usr/bin/env python3

from setuptools import find_packages, setup

with open('./README.md') as fp:
    long_description = fp.read()

setup(
    name='concerns',
    version='1.0',
    provides=['concerns'],
    description='aspect oriented programming helper',
    long_description=long_description,
    long_description_content_type='text/markdown',
    author='ℜodrigo ℭacilhας',
    author_email='batalema@cacilhas.info',
    url='https://bitbucket.org/cacilhas/concerns',
    packages=find_packages(exclude=('tests', 'tests.*')),
    install_requires=[
    ],
    test_suite='tests',
    tests_require=[
    ],
    classifiers=[
        'License :: OSI Approved :: BSD License',
        'Programming Language :: Python :: 3.6',
        'Programming Language :: Python :: 3 :: Only',
        'Programming Language :: Python :: Implementation :: CPython',
        'Topic :: Software Development :: Libraries :: Python Modules',
    ],
)
